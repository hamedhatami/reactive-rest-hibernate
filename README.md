###  Run Postgres docker container

     docker run -d --rm --name my_reative_db -e POSTGRES_USER=user -e POSTGRES_PASSWORD=password -e POSTGRES_DB=my_db -p 5432:5432 postgres

### Add a movie

     curl -i -X POST "http://localhost:8080/movies" -H "Content-Type: application/json" -d '{"title":"movie_title","description":"movie_description"}'

### Test a rollback scenario

     curl -i -X POST "http://localhost:8080/movies" -H "Content-Type: application/json" -d '{"title":"haha","description":"movie_description"}'

### Get all movies back

     curl -i -X GET "http://localhost:8080/movies" -H "Content-Type: application/json"


### Build Docker Image

     mvn clean install -Dquarkus.container-image.build=true -Dmaven.test.skip

### Run the Application
  
    docker run -it --rm --name movie_application -p 8080:8080 reactive-rest-hibernate:1.1.0

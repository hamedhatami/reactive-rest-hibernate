package com.api;

import com.api.entity.Movie;

import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;

import io.quarkus.test.junit.QuarkusTest;
import jakarta.json.bind.Jsonb;
import jakarta.json.bind.JsonbBuilder;
import jakarta.json.bind.JsonbConfig;

import static io.restassured.RestAssured.given;

@QuarkusTest
public class MovieResourceTest {

    static final Jsonb jsonb = JsonbBuilder.create(new JsonbConfig());

    @Test
    @Order(1)
    public void testCreateMovie() {
        Movie movie = Movie
                .builder()
                .title("movie_title")
                .description("movie_description")
                .build();
        given()
                .when()
                .header("Content-Type", "application/json")
                .body(jsonb.toJson(movie))
                .post("/movies")
                .then()
                .statusCode(201);
    }


    @Test
    @Order(2)
    public void testGetAll() {
        given()
                .when()
                .header("Content-Type", "application/json")
                .get("/movies")
                .then()
                .statusCode(200);
    }
}
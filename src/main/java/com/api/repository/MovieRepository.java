package com.api.repository;

import com.api.entity.Movie;
import io.quarkus.hibernate.reactive.panache.Panache;
import io.quarkus.hibernate.reactive.panache.PanacheEntityBase;
import io.quarkus.hibernate.reactive.panache.PanacheRepository;
import io.quarkus.logging.Log;
import io.quarkus.panache.common.Sort;
import io.smallrye.mutiny.Uni;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.ws.rs.core.Response;
import org.hibernate.reactive.mutiny.Mutiny;
import org.jboss.logging.Logger;

import java.time.Duration;
import java.util.Collections;
import java.util.List;

import static jakarta.ws.rs.core.Response.Status.CREATED;


@ApplicationScoped
public class MovieRepository implements PanacheRepository<Movie> {

    private static final Logger LOGGER = Logger.getLogger(MovieRepository.class.getName());

    public Uni<Response> addMovie(final Movie movie) {
        return Panache
                .withTransaction(() -> {
                    if (movie.title.equalsIgnoreCase("haha")) {
                        return Panache.currentTransaction()
                                .invoke(Mutiny.Transaction::markForRollback)
                                .invoke(() -> Log.info("Transaction is marked for roll-back"))
                                .chain(() -> Uni.createFrom().failure(new RuntimeException()));
                    }
                    return persist(movie);
                })
                .replaceWith(Response.ok(movie).status(CREATED)::build)
                .ifNoItem()
                .after(Duration.ofMillis(200))
                .fail()
                .onFailure()
                .transform(t -> new IllegalStateException("Error"));
    }

    public Uni<List<Movie>> getAllMovies() {
        return Movie
                .listAll(Sort.by("title"))
                .ifNoItem()
                .after(Duration.ofMillis(200))
                .fail()
                .onFailure()
                .recoverWithUni(Uni.createFrom().<List<PanacheEntityBase>>item(Collections.EMPTY_LIST));

    }

}

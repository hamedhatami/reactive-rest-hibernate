package com.api.external;

import com.api.entity.Movie;
import com.api.repository.MovieRepository;

import java.util.List;

import io.smallrye.mutiny.Uni;
import jakarta.inject.Inject;
import jakarta.ws.rs.Consumes;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;

@Path("/movies")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class MovieResource {

    @Inject
    MovieRepository movieRepository;

    @POST
    public Uni<Response> add(final Movie movie) {
        return movieRepository.addMovie(movie);
    }

    @GET
    public Uni<List<Movie>> getAllData() {
        return movieRepository.getAllMovies();
    }


}